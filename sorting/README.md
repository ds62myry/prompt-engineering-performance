
## Compiling

Compile the code by running

```bash
mkdir build
cd build
cmake ..
make -j
```

## Run the benchmark
You can now run the built executable each one will print their execution time to stdout.