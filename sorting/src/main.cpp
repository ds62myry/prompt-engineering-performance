#include <iostream>
#include <chrono>

#include <vector>

#if defined (NAIVE)

#include <algorithm>

void sort(std::vector<double> &values) {
  std::sort(values.begin(), values.end());
}

#elif defined (COT)

#include <algorithm>

void sort(std::vector<double> &values) {
    bool change = true;
    size_t n = values.size();

    while (change) {
        change = false;

        for (size_t i = 1; i < n; ++i) {
            if (values[i] < values[i - 1]) {
                std::swap(values[i], values[i - 1]);
                change = true;
            }
        }

        // Reduce the loop boundary
        --n;
    }
}

#elif defined (EXPERT)

void sort(std::vector<double> &values) {
    bool change = false;
    size_t n = values.size();

    do {
        change = false;

        for (size_t i = 0; i < n - 1; ++i) {
            if (values[i] > values[i + 1]) {
                std::swap(values[i], values[i + 1]);
                change = true;
            }
        }

        // After each pass, the largest element is guaranteed to be at the end
        --n;

    } while (change);
}

#else

void sort(std::vector<double> & values) {

  bool change = false;

  do {
    change = false;
    for (size_t i = 1; i < values.size(); ++i) {
      if (values[i] < values[i-1]) {
        double tmp = values[i];
        values[i] = values[i-1];
        values[i-1] = tmp;

        change = true;
      }
    }

  } while (change);

}
#endif

int main(int argc, char ** argv) {

  std::vector<double> values(1 << 14);

  srand(1234);

  for (size_t i = 0; i < values.size(); ++i) {
    values[i] = rand();
  }

  
  auto before_time = std::chrono::high_resolution_clock::now();
  sort(values);
  auto after_time = std::chrono::high_resolution_clock::now();

  std::cout << std::chrono::duration<double, std::ratio<1, 1000>>(after_time - before_time).count() << std::endl;

}