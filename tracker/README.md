
## Compiling

Compile the code by running

```bash
mkdir build
cd build
cmake ..
make -j
```

## Run the benchmark

After compiling the benchmark can be run using.
```bash
./run.sh
```
This wil produce one file for each method containing the execution times.
To get the average times you can use the `avg_time.py` python script.

```bash
python3 ../avg_time.py <file>
```