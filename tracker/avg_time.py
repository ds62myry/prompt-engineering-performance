import sys
import numpy as np

f = open(sys.argv[1])
values = np.array([float(l) for l in f.readlines()])
f.close()

print(np.mean(values))
