#include <stdint.h>

#include <opencv2/core/types.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>

#include <vector>
#include <bitset>
#include <array>

#include <chrono>

#include "math/vector.h"

uint8_t get_robot_id(const cv::Mat & local, const std::tuple<cv::Point2d, cv::Point2d> & yellow_line, double phi) {

  double sin_phi, cos_phi;
  sincos(phi, &sin_phi, &cos_phi); // Get sin and cos in one instruction on CPU. About twice as fast as sin(phi) then cos(phi).

  std::array<uint8_t, 23> line_brightness;
  std::array<uint8_t, 17> possible_ids;

  dezibot::math::Vector<2> yellow_line_start(std::get<0>(yellow_line).x, std::get<0>(yellow_line).y);
  dezibot::math::Vector<2> yellow_line_end(std::get<1>(yellow_line).x, std::get<1>(yellow_line).y);

  double val = (yellow_line_end - yellow_line_start) * (dezibot::math::Vector<2>(sin_phi, cos_phi));

  if (phi >= 0.0 && phi < M_PI) {
    std::swap(yellow_line_end, yellow_line_start);
  }

  for (int dist = 3; dist < 20; ++dist) {

    const dezibot::math::Vector<2> offset(cos_phi * -dist, sin_phi * -dist);
    const dezibot::math::Vector<2> p1 = yellow_line_start + offset;
    const dezibot::math::Vector<2> p2 = yellow_line_end + offset;

    uint32_t sum = 0;

    for (int p = 0; p < 21; ++p) {

      dezibot::math::Vector<2> pos = p1 + (p2 - p1) * (p / 21.0);

      cv::Point sampling_point(round(pos[0]), round(pos[1]));
      cv::Scalar value = local.at<cv::Vec3b>(sampling_point);

      // std::cout << value << std::endl;

      line_brightness[p] = value[1];
      sum += value[1];

    }

    double avg = (double) sum / 21.0;

    uint8_t id = 0;
    for (uint32_t j = 0; j < 7; ++j) {
      uint32_t sum = line_brightness[j*3+1];
      id |= (sum > avg) << (6 - j);
    }

    possible_ids[dist-3] = id;

  }

  uint8_t upper = 0, lower = 0;

  for (uint32_t i = 0; i < 7; ++i) {
    uint32_t total = 0;
    for (uint32_t j = 0; j < 8; ++j) {
      total += (possible_ids[j] >> i) & 0x1;
    }
    if (total > 4) {
      upper |= (1 << i);
    }

    total = 0;
    for (uint32_t j = 0; j < 8; ++j) {
      total += (possible_ids[j+8] >> i) & 0x1;
    }
    if (total > 4) {
      lower |= (1 << i);
    }
  }

  /// Fast way to swap bits in lower to be from right to left.
  uint8_t b = (lower * 0x0202020202ULL & 0x010884422010ULL) % 1023;
  
  std::bitset<14> bits(upper << 7 | (b >> 1));

  uint8_t id = 0;

  for (uint32_t i = 0; i < 7; ++i) {
    if ((bits[i*2] && bits[i*2+1]) || (!bits[i*2] && !bits[i*2+1])) { // Decode error, this is not possible.
      return 255;
    }
    id |= bits[i*2] << i;
  }

  return id;

}