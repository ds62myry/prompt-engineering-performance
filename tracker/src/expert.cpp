#include <opencv2/core.hpp>
#include <array>
#include <bitset>
#include <cmath>

namespace dezibot::math {
    template <int N>
    class Vector {
    public:
        double data[N];
        
        Vector() {}

        Vector(double x, double y) {
            data[0] = x;
            data[1] = y;
        }

        double operator[](int index) const {
            return data[index];
        }

        double& operator[](int index) {
            return data[index];
        }

        Vector<N> operator+(const Vector<N>& other) const {
            Vector<N> result;
            for (int i = 0; i < N; ++i) {
                result[i] = data[i] + other[i];
            }
            return result;
        }

        Vector<N> operator*(double scalar) const {
            Vector<N> result;
            for (int i = 0; i < N; ++i) {
                result[i] = data[i] * scalar;
            }
            return result;
        }
    };
}

uint8_t get_robot_id(const cv::Mat& local, const std::tuple<cv::Point2d, cv::Point2d>& yellow_line, double phi) {
    constexpr int num_samples = 21;
    constexpr int num_segments = 8;

    double sin_phi, cos_phi;
    sincos(phi, &sin_phi, &cos_phi);

    dezibot::math::Vector<2> yellow_line_start(std::get<0>(yellow_line).x, std::get<0>(yellow_line).y);
    dezibot::math::Vector<2> yellow_line_end(std::get<1>(yellow_line).x, std::get<1>(yellow_line).y);

    std::array<uint8_t, 23> line_brightness;
    std::array<uint8_t, 17> possible_ids;

    double val = (yellow_line_end - yellow_line_start) * dezibot::math::Vector<2>(sin_phi, cos_phi);

    if (phi >= 0.0 && phi < M_PI) {
        std::swap(yellow_line_end, yellow_line_start);
    }

    for (int dist = 3; dist < 20; ++dist) {
        const dezibot::math::Vector<2> offset(cos_phi * -dist, sin_phi * -dist);
        const dezibot::math::Vector<2> p1 = yellow_line_start + offset;
        const dezibot::math::Vector<2> p2 = yellow_line_end + offset;

        uint32_t sum = 0;

        for (int p = 0; p < num_samples; ++p) {
            dezibot::math::Vector<2> pos = p1 + (p2 - p1) * (p / static_cast<double>(num_samples));
            cv::Point sampling_point(static_cast<int>(round(pos[0])), static_cast<int>(round(pos[1])));
            cv::Scalar value = local.at<cv::Vec3b>(sampling_point);
            line_brightness[p] = value[1];
            sum += value[1];
        }

        double avg = static_cast<double>(sum) / num_samples;

        uint8_t id = 0;
        for (uint32_t j = 0; j < num_segments; ++j) {
            uint32_t sum = line_brightness[j * 3 + 1];
            id |= (sum > avg) << (num_segments - 1 - j);
        }

        possible_ids[dist - 3] = id;
    }

    uint8_t upper = 0, lower = 0;

    for (uint32_t i = 0; i < num_segments; ++i) {
        uint32_t total = 0;
        for (uint32_t j = 0; j < num_segments + 1; ++j) {
            total += (possible_ids[j] >> i) & 0x1;
        }
        if (total > num_segments / 2) {
            upper |= (1 << i);
        }

        total = 0;
        for (uint32_t j = 0; j < num_segments; ++j) {
            total += (possible_ids[j + num_segments] >> i) & 0x1;
        }
        if (total > num_segments / 2) {
            lower |= (1 << i);
        }
    }

    // Fast way to swap bits in lower to be from right to left.
    uint8_t b = (lower * 0x0202020202ULL & 0x010884422010ULL) % 1023;

    std::bitset<14> bits(upper << 7 | (b >> 1));

    uint8_t id = 0;

    for (uint32_t i = 0; i < num_segments; ++i) {
        if ((bits[i * 2] && bits[i * 2 + 1]) || (!bits[i * 2] && !bits[i * 2 + 1])) {
            // Decode error, this is not possible.
            return 255;
        }
        id |= bits[i * 2] << i;
    }

    return id;
}