#include <stdint.h>

#include <opencv2/core/types.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>

#include <vector>
#include <bitset>
#include <array>

#include <chrono>

#include "math/vector.h"


#define MIN_MARKER_AREA 32
#define MAX_MARKER_AREA 4000

#define MIN_CONTOUR_AREA 150
#define MIN_BOUNDING_BOX_AREA 100

#define MAX_CONTOUR_AREA 4000
#define MAX_BOUNDING_BOX_AREA 4000

#define BLUE_LOWER cv::Scalar(85, 100, 100)
#define BLUE_UPPER cv::Scalar(115, 255, 255)

#define YELLOW_LOWER cv::Scalar(25, 40, 80)
#define YELLOW_UPPER cv::Scalar(75, 255, 255)

struct robot_info {
  uint8_t robot_id;
  double x;
  double y;
  double phi;
  double px;
  double py;
};

struct image {
  image() = default;

  image(uint32_t width, uint32_t height, uint32_t channel_count, uint8_t * data) : width(width), height(height), channel_count(channel_count), data(data) {
      
  }

  uint32_t width;
  uint32_t height;
  uint32_t channel_count;
  uint8_t * data;

  inline uint8_t operator()(int x, int y, int c) const {
    return data[(x + y * width) * channel_count + c];
  }

};

struct camera_info {

  camera_info();
  camera_info(uint32_t width, uint32_t height);

  std::array<double, 9> matrix;
  std::array<double, 5> dist_coefs;
};

struct homography_data_t {

  homography_data_t() = default;

  cv::Mat h;

};

extern uint8_t get_robot_id(const cv::Mat & local, const std::tuple<cv::Point2d, cv::Point2d> & yellow_line, double phi);

bool find_longest_contour_with_color(cv::Mat & image, cv::Scalar min, cv::Scalar max, std::vector<cv::Point> & res) {

  cv::Mat hsv, mask;
  cv::cvtColor(image, hsv, cv::COLOR_BGR2HSV);
  cv::inRange(hsv, min, max, mask);

  std::vector<std::vector<cv::Point>> contours;
  cv::findContours(mask, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

  // std::cout << "Found " << contours.size() << " candidates for contours" << std::endl;

  if (contours.size() > 1) {
    double l = 0;
    double cl = 0;
    size_t max_index = 0;
    for (size_t i = 0; i < contours.size(); ++i) {
      cl = cv::arcLength(contours[i], true);
      if (cl > l) {
        l = cl;
        max_index = i;
      }
    }
    res = contours[max_index];
    return true;
  }

  if (contours.size()) {
    res = contours[0];
    return true;
  }

  return false;

}

std::tuple<cv::Point2d, cv::Point2d> get_center_line(std::vector<cv::Point> & contour) {

  cv::RotatedRect yellow_rect = cv::minAreaRect(contour);
  std::vector<cv::Point2f> yellow_corners(4);
  yellow_rect.points(yellow_corners.data());

  // cv::boxPoints(yellow_rect, yellow_corners.data());

  double min_dist = std::numeric_limits<double>::max();
  size_t closest;
  for (size_t i = 1; i <= 3; ++i) {
    double dist = sqrt((yellow_corners[0].x - yellow_corners[i].x) * (yellow_corners[0].x - yellow_corners[i].x) + (yellow_corners[0].y - yellow_corners[i].y) * (yellow_corners[0].y - yellow_corners[i].y));
    if (dist < min_dist) {
      min_dist = dist;
      closest = i;
    }
  }

  size_t rest_1, rest_2;

  switch (closest) {
    case 1:
      rest_1 = 2;
      rest_2 = 3;
      break;
    case 2:
      rest_1 = 1;
      rest_2 = 3;
      break;
    case 3:
      rest_1 = 1;
      rest_2 = 2;
      break;
  }

  cv::Point2d first = cv::Point2d((yellow_corners[0].x + yellow_corners[closest].x) / 2, (yellow_corners[0].y + yellow_corners[closest].y) / 2);
  cv::Point2d last = cv::Point2d((yellow_corners[rest_1].x + yellow_corners[rest_2].x) / 2, (yellow_corners[rest_1].y + yellow_corners[rest_2].y) / 2);

  return std::make_tuple(first, last);

}

std::vector<robot_info> find_robots(const cv::Mat & frame, camera_info & cam_info, const homography_data_t * h_data) {

  static const std::vector<cv::Point2d> marker_coords = {
    // cv::Point2d(1000, -500),
    // cv::Point2d(1000, 500),
    // cv::Point2d(-1000, 500),
    // cv::Point2d(-1000, -500)

    cv::Point2d(994.9, -502.5),
    cv::Point2d(994.9, 507.5),
    cv::Point2d(-985.1, 497.5),
    cv::Point2d(-985.1, -502.5)

  };

  static std::vector<cv::Point2d> dst_points = {
    // cv::Point2d(1000, -500),
    // cv::Point2d(1000, 500),
    // cv::Point2d(-1000, 500),
    // cv::Point2d(-1000, -500)

    cv::Point2d(994.9, -502.5),
    cv::Point2d(994.9, 507.5),
    cv::Point2d(-985.1, 497.5),
    cv::Point2d(-985.1, -502.5)

  };

  static std::vector<cv::Point2d> src_points = {
    cv::Point2d(0.5,-0.5),
    cv::Point2d(0.5,0.5),
    cv::Point2d(-0.5,-0.5),
    cv::Point2d(-0.5, 0.5)
  };

  // cv::Mat frame(img.height, img.width, CV_8UC3, img.data);
  cv::Mat camera_matrix;
  camera_matrix = cv::Mat(3, 3, CV_64F, cam_info.matrix.data());
  // std::cout << "Camera_matrix " << camera_matrix << std::endl;
  cv::Mat dist_coefs = cv::Mat(1, cam_info.dist_coefs.size(), CV_64F, cam_info.dist_coefs.data());

  // if (!has_homography_markers) {
  //   src_points = find_homography_markers(frame, camera_matrix, dist_coefs);
  //   if (src_points.size() != 4) {
  //     std::cerr << "Wrong number of homography markers found: " << src_points.size() << std::endl;
  //     return {};
  //   } else {
  //     std::cerr << "Found homography markers" << std::endl;
  //     std::ofstream marker_file("found_markers.txt"); 
  //     for (size_t i = 0; i < src_points.size(); ++i) {
  //       auto & p = src_points[i];
  //       // dst_points[i].x = 1000 * sign(p.x);
  //       // dst_points[i].y = -500 * sign(p.y);
  //       if (p.x > 0 && p.y > 0) {
  //         dst_points[i] = marker_coords[0];
  //       } else if (p.x > 0 && p.y < 0) {
  //         dst_points[i] = marker_coords[1];
  //       } else if (p.x < 0 && p.y < 0) {
  //         dst_points[i] = marker_coords[2];
  //       } else {
  //         dst_points[i] = marker_coords[3];
  //       }
  //       std::cerr << "mapping " << p << " to " << dst_points[i] << std::endl;
  //       marker_file << p.x << " " << p.y << " : " << dst_points[i].x << " " << dst_points[i].y << "\n";
  //     }
  //     h_data->h = cv::findHomography(src_points, dst_points);
  //     has_homography_markers = true;
  //     std::cout << h_data->h << std::endl;
  //     dezibot::tracking::set_border_leds({0,0,0,0});
  //     marker_file.close();
  //   }
  // }

  std::vector<std::vector<cv::Point>> contours;

  // std::cerr << h << std::endl;
  
  cv::Mat binFrame;
  cv::cvtColor(frame, binFrame, cv::COLOR_BGR2GRAY);
  cv::threshold(binFrame, binFrame, 64, 255, cv::THRESH_BINARY);

  cv::findContours(binFrame, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);

  cv::Mat contour_image(frame.size(), CV_8UC3, cv::Scalar(0,0,0));

  std::vector<robot_info> infos(contours.size());

  // std::cerr << "Found " << contours.size() << " contours in image" << std::endl;

  //#pragma omp parallel for schedule(dynamic)
  for (size_t i = 0; i < contours.size(); ++i) {
    // cv::drawContours(contour_image, contours, i, cv::Scalar(255, 255, 255));

    double area = cv::contourArea(contours[i]);

    if (area < MIN_CONTOUR_AREA || area > MAX_CONTOUR_AREA) {
      continue;
    }

    cv::Rect bbox = cv::boundingRect(contours[i]);
    double bbox_area = bbox.area();

    if (bbox_area < MIN_BOUNDING_BOX_AREA || bbox_area > MAX_BOUNDING_BOX_AREA) {
      continue;
    }

    cv::Mat local = frame(bbox);
    std::vector<cv::Point> yellow_contour;
    bool has_yellow = find_longest_contour_with_color(local, YELLOW_LOWER, YELLOW_UPPER, yellow_contour);
    std::vector<cv::Point> blue_contour;
    bool has_blue = find_longest_contour_with_color(local, BLUE_LOWER, BLUE_UPPER, blue_contour);

    // cv::imwrite("bbox.png", local);

    // std::cout << "Has contours: " << has_blue << " " << has_yellow << std::endl;

    if (!has_blue || !has_yellow) {
      continue;
    }

    double blue_length = cv::arcLength(blue_contour, true);
    double yellow_length = cv::arcLength(yellow_contour, true);

    // std::cout << "lengths " << blue_length << " " << yellow_length << std::endl;

    if (yellow_length < 15 || blue_length < 15) {
      continue;

    }

    auto blue_moments = cv::moments(blue_contour);
    if (blue_moments.m00 == 0.0) {
      continue;
    }

    double blue_center_x = blue_moments.m10 / blue_moments.m00;
    double blue_center_y = blue_moments.m01 / blue_moments.m00;

    auto yellow_moments = cv::moments(yellow_contour);
    if (yellow_moments.m00 == 0.0) {
      continue;
    }

    double yellow_center_x = yellow_moments.m10 / yellow_moments.m00;
    double yellow_center_y = yellow_moments.m01 / yellow_moments.m00;

    // std::cout << "Center at " << bbox.x + blue_center_x << " " << bbox.y + blue_center_y << std::endl;

    std::tuple<cv::Point2d, cv::Point2d> line_points = get_center_line(yellow_contour);

    // This needs to be rotated by pi/2. Thus the different signs than expected
    double line_dx = std::get<0>(line_points).y - std::get<1>(line_points).y;
    double line_dy = std::get<1>(line_points).x - std::get<0>(line_points).x;

    double dot = (yellow_center_x - blue_center_x) * line_dx + (yellow_center_y - blue_center_y) * line_dy;

    // double phi = sign(dot) * atan2(line_dy, line_dx);
    double phi = atan2(line_dy, line_dx);
    if (dot < 0) {
      phi = -M_PI + phi;
    }

    auto before_time = std::chrono::high_resolution_clock::now();
    uint8_t robot_id = get_robot_id(local, line_points, phi);
    auto after_time = std::chrono::high_resolution_clock::now();

    std::cout << std::chrono::duration<double, std::ratio<1, 1000000>>(after_time - before_time).count() << "\n";

    // std::cout << " id: " << (int) robot_id << " position: " << bbox.x + blue_center_x << " " << bbox.y + blue_center_y << " angle: " << phi << " dot " << dot << std::endl;

    cv::Point2f center(bbox.x + blue_center_x, bbox.y + blue_center_y);
    cv::Mat undistorted_points;

    cv::Mat_<cv::Point2f> dist_points(1, 1);
    dist_points(0) = center;

    cv::undistortPoints(dist_points, undistorted_points, camera_matrix, dist_coefs);
    cv::perspectiveTransform(undistorted_points, undistorted_points, h_data->h);

    center = undistorted_points.at<cv::Point2f>(0);

    // infos.push_back({robot_id, bbox.x + blue_center_x, bbox.y + blue_center_y, phi});
    infos[i] = {robot_id, center.x, center.y, -phi, bbox.x + yellow_center_x, bbox.y + yellow_center_y};

  }

  return infos;

}

camera_info::camera_info() {
  matrix = {
    1,0,0,
    0, 1, 0,
    0, 0, 1
  };

  dist_coefs = {
    0,0,0,0
  };

}

camera_info::camera_info(uint32_t width, uint32_t height) {
  double cx = width / 2.0;
  double cy = height / 2.0;

  matrix = {
    1,0, cx,
    0, 1, cy,
    0, 0, 1
  };

  dist_coefs = {
    0,0,0,0
  };
}

void set_homography_markers(const std::array<dezibot::math::Vector<2>, 4> & points, camera_info & cam_info, homography_data_t * h_data) {

  static const std::vector<cv::Point2d> dst_points = {
    // cv::Point2d(1000, -500),
    // cv::Point2d(1000, 500),
    // cv::Point2d(-1000, 500),
    // cv::Point2d(-1000, -500)

    cv::Point2d(994.9, -502.5),
    cv::Point2d(994.9, 507.5),
    cv::Point2d(-985.1, 497.5),
    cv::Point2d(-985.1, -502.5)

  };

  std::vector<cv::Point2d> src_points(4);
  
  for (size_t i = 0; i < points.size(); ++i) {
    src_points[i] = cv::Point2d(points[i].x(), points[i].y());
  }

  cv::Mat camera_matrix = cv::Mat(3, 3, CV_64F, cam_info.matrix.data());
  cv::Mat dist_coefs = cv::Mat(1, cam_info.dist_coefs.size(), CV_64F, cam_info.dist_coefs.data());

  cv::undistortPoints(src_points, src_points, camera_matrix, dist_coefs);

  h_data->h = cv::findHomography(src_points, dst_points);

  // has_homography_markers = true;

}

int main(int argc, char ** argv) {

  const uint32_t width  = 3840;
  const uint32_t height = 2160;

  homography_data_t h_data;
  camera_info cam_info(width, height);
  set_homography_markers({
      dezibot::math::Vector<2>(width, height),
      dezibot::math::Vector<2>(width, 0),
      dezibot::math::Vector<2>(0, 0),
      dezibot::math::Vector<2>(0, height)
    }, cam_info, &h_data);


  cv::Mat frame;
  frame = cv::imread("../test.png", cv::IMREAD_COLOR);

  if (frame.empty()) {
    return -1;
  }

  // std::cout << "Image has " << frame.channels() << " channels" << std::endl;

  image img(width, height, frame.channels(), frame.data);

  for (size_t i = 0; i < 200; ++i) {
    auto robots = find_robots(frame, cam_info, &h_data);
  }


}