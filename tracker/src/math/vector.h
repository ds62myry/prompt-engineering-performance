#ifndef VECTOR_H
#define VECTOR_H

#include <cstdarg>
#include <cmath>
#include <ostream>
#include <iostream>

#include "math_exception.h"

// #ifdef __SSE2__

// #include <x86intrin.h>
// #include <immintrin.h>

// #endif

namespace dezibot::math {

template<unsigned int dim, typename T = double> class Vector {

    public:
        /**This will copy data, you can free it after the call*/
        Vector(const T * data){
            for (unsigned int i = 0; i < dim; ++i) {
                this->data[i] = data ? data[i] : (T) 0;
            }
        }

        template <unsigned int l> Vector(Vector<l, T> vec) {
            for (unsigned int i = 0; i < dim; ++i) {
                this->data[i] = (i < l) ? vec[i] : (T) 0;
            }
        }

        Vector(std::vector<T> data) {

            if (data.size() != dim) {
                throw trace_exception("Wrong dimension in initializer");
            }

            for (unsigned int i = 0; i < dim; ++i) {
                this->data[i] = data[i];
            }

        }

        Vector() {
            for (unsigned int i = 0; i < dim; ++i)
                this->data[i] = (T) 0;
        }

        /*Vector(T t0, ...) {

            std::cout << "Creating vector from initialiser list" << std::endl;

            this->data[0] = t0;
            va_list vl;
            va_start(vl, t0);
            for (int i = 1; i < dim; ++i) {
                std::cout << "Loading argument " << i << std::endl;
                T tmp = va_arg(vl, T);
                this->data[i] = tmp;
            }
            va_end(vl);

            std::cout << "Done" << std::endl;

        }*/

        virtual ~Vector() {

        }

        T& operator[](unsigned int i) {
            if (i < 0 || i >= dim)
                throw trace_exception("No such coordinate");
            return data[i];
        }

        T operator*(const Vector<dim, T> vec) const {
            T val = (T) 0;
            for (unsigned int i = 0; i < dim; ++i) {
                val = val + this->data[i] * vec.data[i];
            }
            return val;
        }

        Vector<dim, T> operator+(const Vector<dim, T> vec) const {

            T tmp[dim];
            for (unsigned int i = 0; i < dim; ++i) {
                tmp[i] = data[i] + vec.data[i];
            }
            return Vector<dim, T>(tmp);

        }

        Vector<dim, T> operator*(const T v) const {

            T tmp[dim];
            for (unsigned int i = 0; i < dim; ++i) {
                tmp[i] = data[i] *v;
            }
            return Vector<dim, T>(tmp);

        }

        Vector<dim, T> operator-(const Vector<dim, T> vec) const {

            T tmp[dim];
            for (unsigned int i = 0; i < dim; ++i) {
                tmp[i] = data[i] - vec.data[i];
            }
            return Vector<dim, T>(tmp);

        }

        Vector<dim, T> operator/(const T v) const {

            T tmp[dim];
            for (unsigned int i = 0; i < dim; ++i) {
                tmp[i] = data[i] / v;
            }
            return Vector<dim, T>(tmp);

        }

        void normalize() {
            T val = this->length();
            for (unsigned int i = 0; i < dim; ++i) {
                this->data[i] /= val;
            }
        }

        T length() {
            T sum = (T) 0;
            for (unsigned int i = 0; i < dim; ++i) {
                sum += this->data[i] * this->data[i];
            }
            return (T) sqrt(sum);
        }

        const T * getData() {
            return data;
        }

    protected:

    private:

        T data[dim];

};

template <typename T> Vector<3, T> cross(Vector<3, T> vec1, Vector<3, T> vec2) {

    T tmp[3] = {vec1[1] * vec2[2] - vec1[2] * vec2[1], -vec1[2] * vec2[0] + vec1[0] * vec2[2], vec1[0] * vec2[1] - vec1[1] * vec2[0]};

    return Vector<3, T>(tmp);

}

}

template <unsigned int dim, typename T> inline dezibot::math::Vector<dim, T> operator*(T a, dezibot::math::Vector<dim, T> vec) {
    T tmp[dim];
    for (unsigned int i = 0; i < dim; ++i) {
        tmp[i] = vec[i] * a;
    }
    return dezibot::math::Vector<dim, T>(tmp);
}

template <unsigned int dim, typename T> inline std::ostream& operator<<(std::ostream& stream, dezibot::math::Vector<dim, T> vec) {
    stream << "(";
    stream << vec[0];
    for (unsigned int i = 1; i < dim; ++i) {
        stream << ", " << vec[i];
    }
    stream << ")";
    return stream;
}

#ifdef __clang__  /// All of this is needed to make clangd work, such a waste of 3 hours...
#undef __SSE2__

#warning "Please use g++ to compile this file"

namespace dezibot::math {

typedef struct {
    double v[2];
} __m128d;

__m128d _mm_add_pd(__m128d, __m128d);
__m128d _mm_sub_pd(__m128d, __m128d);
__m128d _mm_mul_pd(__m128d, __m128d);
__m128d _mm_div_pd(__m128d, __m128d);
__m128d _mm_load_pd1(const double *P);
__m128d _mm_load_pd(const double *P);
__m128d _mm_store_pd(double *P, __m128d A);

template <> class Vector<2, double> {
  public:
    Vector() : Vector(0,0) {

    }

    Vector(double x, double y) {

      ((double *) &vData)[0] = x;
      ((double *) &vData)[1] = x;

    }

    Vector(__m128d d): vData(d) {

    }

    inline double & operator[] (size_t i) {
      return ((double *) &vData)[i];
    }

    inline const double & operator() (size_t i) const {
      return ((double *) &vData)[i];
    }

    inline Vector<2, double> operator+(const Vector<2, double> & other) const {
      return _mm_add_pd(vData, other.vData);
    }

    inline Vector<2, double> operator-(const Vector<2, double> & other) const {
      return _mm_sub_pd(vData, other.vData);
    }

    inline Vector<2, double> operator*(double d) const {
      return _mm_mul_pd(vData, _mm_load_pd1(&d));
    }

    inline Vector<2, double> operator/(double d) const {
      return _mm_div_pd(vData, _mm_load_pd1(&d));
    }

    inline double operator*(const Vector<2, double> & other) const {
      alignas(16) double a[2];
      _mm_store_pd(a, _mm_mul_pd(vData, other.vData));
      return a[0] + a[1];
    }

    inline double length() const {
      __m128d tmp = _mm_mul_pd(vData, vData);
      return sqrt(((double *) &tmp)[0] + ((double *) &tmp)[1]);
    }

    void normalize() {
      double len = length();
      vData = _mm_div_pd(vData, _mm_load_pd1(&len));
    }

    inline double x() const {
      return ((double *) &vData)[0];
    }

    inline double y() const {
      return ((double *) &vData)[1];
    }

  private:
    __m128d vData;
};

}

#endif

#ifdef __SSE2__
//#pragma message ("Using SSE2 instructions for vectors")
#include <immintrin.h>

namespace dezibot::math {

template <> class Vector<2, double> {
  public:
    Vector() : Vector(0,0) {

    }

    Vector(double x, double y) {

      ((double *) &vData)[0] = x;
      ((double *) &vData)[1] = y;

    }

    Vector(__m128d d): vData(d) {

    }

    inline double & operator[] (size_t i) {
      return ((double *) &vData)[i];
    }

    inline const double & operator() (size_t i) const {
      return ((double *) &vData)[i];
    }

    inline Vector<2, double> operator+(const Vector<2, double> & other) const {
      return _mm_add_pd(vData, other.vData);
    }

    inline Vector<2, double> operator-(const Vector<2, double> & other) const {
      return _mm_sub_pd(vData, other.vData);
    }

    inline Vector<2, double> operator*(double d) const {
      return _mm_mul_pd(vData, _mm_load_pd1(&d));
    }

    inline Vector<2, double> operator/(double d) const {
      return _mm_div_pd(vData, _mm_load_pd1(&d));
    }

    inline double operator*(const Vector<2, double> & other) const {
      alignas(16) double a[2];
      _mm_store_pd(a, _mm_mul_pd(vData, other.vData));
      return a[0] + a[1];
    }

    inline double length() const {
      __m128d tmp = _mm_mul_pd(vData, vData);
      return sqrt(((double *) &tmp)[0] + ((double *) &tmp)[1]);
    }

    void normalize() {
      double len = length();
      vData = _mm_div_pd(vData, _mm_load_pd1(&len));
    }

    inline double x() const {
      return ((double *) &vData)[0];
    }

    inline double y() const {
      return ((double *) &vData)[1];
    }

  private:
    __m128d vData;
};

template <> class Vector<4, float> {

    public:

        Vector() : Vector(0,0,0,0) {

        }

        template <unsigned int l> Vector(Vector<l, float> vec) {

            alignas(16) float data[4];

            for (unsigned int i = 0; i < 4; ++i) {
                data[i] = (i < l) ? vec[i] : 0;
            }
            vData = _mm_load_ps(data);
        }

        Vector(float x, float y, float z, float w) {

            float data[4];

            data[0] = x;
            data[1] = y;
            data[2] = z;
            data[3] = w;

            vData = _mm_load_ps(data);

        }

        Vector(std::vector<float> data) {

            if (data.size() != 4) {
                throw trace_exception("Wrong dimension in initializer");
            }

            vData = _mm_load_ps(data.data());

        }

        Vector(float * data) {

            alignas(16) float tmp[4];

            tmp[0] = data[0];
            tmp[1] = data[1];
            tmp[2] = data[2];
            tmp[3] = data[3];

            vData = _mm_load_ps(tmp);

        }

        Vector(const __m128 vData) {
            this->vData = vData;
        }

        /*const float * getData() {
            return data;
        }*/

        __m128 getVData() {
            return vData;
        }

        const float operator[](int i) {
            if (i < 0 || i >= 4)
                throw trace_exception("No such coordinate");

            float data[4];
            _mm_store_ps(data, vData);
            return data[i];
        }

        float operator*(Vector<4, float> vec) const {

            /*__m128 v0 = _mm_mul_ps(vData, vec.vData);
            v0 = _mm_add_ps(_mm_shuffle_ps(v0, v0, _MM_SHUFFLE(2, 3, 0, 1)), vec.vData);
            v0 = _mm_add_ps(v0, _mm_shuffle_ps(v0, v0, _MM_SHUFFLE(0, 1, 2, 3)));

            float tmp[4];

            _mm_store_ps(tmp, v0);

            return tmp[0];*/

            alignas(16) float a[4];
            alignas(16) float b[4];

            _mm_store_ps(a, vData);
            _mm_store_ps(b, vec.vData);

            return a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3];

        }

        Vector<4, float> operator+(Vector<4, float> vec) const {

            return Vector<4, float>(_mm_add_ps(vData, vec.vData));

        }

        Vector<4, float> operator*(float v) const {

            __m128 vv = _mm_load_ps1(&v);

            return Vector<4, float>(_mm_mul_ps(vData, vv));

        }

        Vector<4, float> operator-(Vector<4, float> vec) const {

            return Vector<4, float>(_mm_sub_ps(vData, vec.vData));

        }

        Vector<4, float> operator/(float v) const {

            __m128 vv = _mm_load_ps1(&v);
            return Vector<4, float>(_mm_div_ps(vData, vv));

        }

        void normalize() {
            float val = this->length();
            __m128 vv = _mm_load_ps1(&val);
            this->vData = _mm_div_ps(vData, vv);
        }

        float length() {

            __m128 tmp = _mm_mul_ps(vData, vData);

            float data[4];
            _mm_store_ps(data, tmp);

            return (float) sqrt(data[0] + data[1] + data[2] + data[3]);
        }

    private:
        //float data[4];
        __m128 vData;

};

}
#endif

#endif // VECTOR_H
