
## Compiling

Compile the code by running

```bash
mkdir build
cd build
cmake ..
make -j
```

## Run the benchmark

After compiling the benchmark can be run using.
```
./run.sh
```
This wil produce one file for each method containing the execution times and the problem sizes.