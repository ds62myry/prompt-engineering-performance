#include <stdio.h>
#include <chrono>
#include <iostream>
#include <vector>

#ifdef NAIVE

#include <immintrin.h>

float weighted_avg(size_t count, float *weights, float *values) {
    // Ensure that the count is a multiple of 4 for better vectorization
    size_t count4 = count / 4 * 4;

    __m128 sum = _mm_setzero_ps();

    // Process four elements at a time using SIMD
    for (size_t i = 0; i < count4; i += 4) {
        __m128 w = _mm_loadu_ps(weights + i);
        __m128 v = _mm_loadu_ps(values + i);
        sum = _mm_add_ps(sum, _mm_mul_ps(w, v));
    }

    // Horizontal sum of the four accumulated values
    sum = _mm_hadd_ps(sum, sum);
    sum = _mm_hadd_ps(sum, sum);

    // Extract the result from the SIMD register
    float result;
    _mm_store_ss(&result, sum);

    // Process any remaining elements
    for (size_t i = count4; i < count; ++i) {
        result += weights[i] * values[i];
    }

    return result / count;
}

#elif defined (COT)

#include <immintrin.h>

float weighted_avg(size_t count, float *weights, float *values) {
    __m256 sum_vec = _mm256_setzero_ps();
    size_t i;

    for (i = 0; i < count - 7; i += 8) {
        __m256 weights_vec = _mm256_loadu_ps(weights + i);
        __m256 values_vec = _mm256_loadu_ps(values + i);
        sum_vec = _mm256_add_ps(sum_vec, _mm256_mul_ps(weights_vec, values_vec));
    }

    float sum = 0;
    // Horizontal sum of the vector
    sum_vec = _mm256_hadd_ps(sum_vec, sum_vec);
    sum_vec = _mm256_hadd_ps(sum_vec, sum_vec);
    sum += _mm256_cvtss_f32(sum_vec);

    // Handle remaining elements
    for (; i < count; ++i) {
        sum += weights[i] * values[i];
    }

    return sum / count;
}


#elif defined (EXPERT)

#include <stddef.h>
float weighted_avg(size_t count, const float *weights, const float *values) {
  float sum = 0.0f;

  // Unroll the loop to improve performance
  size_t i;
  for (i = 0; i < count - 3; i += 4) {
    sum += weights[i] * values[i];
    sum += weights[i + 1] * values[i + 1];
    sum += weights[i + 2] * values[i + 2];
    sum += weights[i + 3] * values[i + 3];
  }

  // Handle the remaining elements
  for (; i < count; ++i) {
    sum += weights[i] * values[i];
  }

  return sum / count;
}

#elif defined (MANUAL)

#include <immintrin.h>

float weighted_avg(size_t count, float * weights, float * values) {

  __m256 res1 = _mm256_setzero_ps();
  __m256 res2 = _mm256_setzero_ps();
  __m256 res3 = _mm256_setzero_ps();
  __m256 res4 = _mm256_setzero_ps();

  __m256 res5 = _mm256_setzero_ps();
  __m256 res6 = _mm256_setzero_ps();
  __m256 res7 = _mm256_setzero_ps();
  __m256 res8 = _mm256_setzero_ps();

  for (size_t i = 0; i < count;) {
    res1 = _mm256_fmadd_ps(_mm256_load_ps(&weights[i]), _mm256_load_ps(&values[i]), res1);
    i+=8;
    res2 = _mm256_fmadd_ps(_mm256_load_ps(&weights[i]), _mm256_load_ps(&values[i]), res2);
    i+=8;
    res3 = _mm256_fmadd_ps(_mm256_load_ps(&weights[i]), _mm256_load_ps(&values[i]), res3);
    i+=8;
    res4 = _mm256_fmadd_ps(_mm256_load_ps(&weights[i]), _mm256_load_ps(&values[i]), res4);
    i+=8;

    res5 = _mm256_fmadd_ps(_mm256_load_ps(&weights[i]), _mm256_load_ps(&values[i]), res5);
    i+=8;
    res6 = _mm256_fmadd_ps(_mm256_load_ps(&weights[i]), _mm256_load_ps(&values[i]), res6);
    i+=8;
    res7 = _mm256_fmadd_ps(_mm256_load_ps(&weights[i]), _mm256_load_ps(&values[i]), res7);
    i+=8;
    res8 = _mm256_fmadd_ps(_mm256_load_ps(&weights[i]), _mm256_load_ps(&values[i]), res8);
    i+=8;
  }

  res1 = _mm256_add_ps(res1, res2);
  res3 = _mm256_add_ps(res3, res4);
  res5 = _mm256_add_ps(res5, res6);
  res7 = _mm256_add_ps(res7, res8);

  res1 = _mm256_add_ps(res1, res3);
  res5 = _mm256_add_ps(res5, res7);

  res1 = _mm256_add_ps(res1, res5);
  
  float * res = (float *) &res1;

  return (res[0] + res[1] + res[2] + res[3] + res[4] + res[5] + res[6] + res[7]) / count;

}

#elif defined(SHUFFLE)

#include <immintrin.h>

float weighted_avg(size_t count, float * weights_and_values) {

  __m256 res1 = _mm256_setzero_ps();
  __m256 res2 = _mm256_setzero_ps();
  __m256 res3 = _mm256_setzero_ps();
  __m256 res4 = _mm256_setzero_ps();

  for (size_t i = 0; i < count * 2;) {
    res1 = _mm256_fmadd_ps(_mm256_load_ps(&weights_and_values[i]), _mm256_load_ps(&weights_and_values[i+8]), res1);
    i+=16;
    res2 = _mm256_fmadd_ps(_mm256_load_ps(&weights_and_values[i]), _mm256_load_ps(&weights_and_values[i+8]), res2);
    i+=16;
    res3 = _mm256_fmadd_ps(_mm256_load_ps(&weights_and_values[i]), _mm256_load_ps(&weights_and_values[i+8]), res3);
    i+=16;
    res4 = _mm256_fmadd_ps(_mm256_load_ps(&weights_and_values[i]), _mm256_load_ps(&weights_and_values[i+8]), res4);
    i+=16;
  }

  res1 = _mm256_add_ps(res1, res2);
  res3 = _mm256_add_ps(res3, res4);

  res1 = _mm256_add_ps(res1, res3);

  float * res = (float *) &res1;

  return (res[0] + res[1] + res[2] + res[3] + res[4] + res[5] + res[6] + res[7]) / count;

}

#elif defined (NAIVE_COMPILER)

float weighted_avg(size_t count, float *weights, float *values) {
    float sum = 0.0f;

    // Unroll the loop to process multiple elements in each iteration
    size_t i;
    for (i = 0; i < count - 3; i += 4) {
        sum += weights[i] * values[i] +
               weights[i + 1] * values[i + 1] +
               weights[i + 2] * values[i + 2] +
               weights[i + 3] * values[i + 3];
    }

    // Process the remaining elements
    for (; i < count; ++i) {
        sum += weights[i] * values[i];
    }

    return sum / count;
}

#elif defined(EXPERT_COMPILER)

float weighted_avg(size_t count, float *weights, float *values) {
  size_t i;
  float sum = 0;

  for (i = 0; i < count; i += 4) {
    sum += weights[i] * values[i];
    sum += weights[i + 1] * values[i + 1];
    sum += weights[i + 2] * values[i + 2];
    sum += weights[i + 3] * values[i + 3];
  }

  // Handle remaining elements (if count is not a multiple of 4)
  for (; i < count; ++i) {
    sum += weights[i] * values[i];
  }

  return sum / count;
}

#else

float weighted_avg(size_t count, float * weights, float * values) {

  float sum = 0;
  for (size_t i = 0; i < count; ++i) {
    sum += weights[i] * values[i];
  }
  return sum / count;

}

#endif

#define TEST_SIZE ((size_t)1 << 30)

#include <thread>
#include <array>

// using std::chrono_literals;

int main(int argc, char ** argv) {

  using namespace std::chrono_literals;

#ifdef MANUAL
  std::vector<__m256> weights_vec(TEST_SIZE * sizeof(float) / sizeof(__m256));
  std::vector<__m256> values_vec(TEST_SIZE * sizeof(float) / sizeof(__m256));
#elif defined (SHUFFLE)
  std::vector<__m256> weights_vec(TEST_SIZE * 2 * sizeof(float) / sizeof(__m256));
  // float * weights = (float *) aligned_alloc(512, TEST_SIZE * 2 * sizeof(float));
  std::vector<__m256> values_vec;
#else
  std::vector<float> weights_vec(TEST_SIZE);
  std::vector<float> values_vec(TEST_SIZE);
#endif

// #ifndef SHUFFLE
  float * weights = (float*) weights_vec.data();
// #endif
  float * values = (float*) values_vec.data();

  srand(1234);

  #ifdef SHUFFLE

  for (size_t i = 0; i < TEST_SIZE * 2; i+= 16) {
    for (size_t j = 0; j < 8; ++j) {
      weights[i+j] = (float) rand() / (float)RAND_MAX;
      weights[i+j+8] = (float) rand() / (float)RAND_MAX;
    }
    // weights[(i/8)*16+i%8] = (float) rand() / (float)RAND_MAX;
    // weights[(i/8)*16+i%8+8] = (float) rand() / (float)RAND_MAX;
  }

  #else
  for (size_t i = 0; i < TEST_SIZE; ++i) {
    weights[i] = (float) rand() / (float)RAND_MAX;
    values[i] = (float) rand() / (float)RAND_MAX;
  }
  #endif

  std::cout << "size,time,result" << std::endl;

  std::array<double, 200> times;
  float total_res = 0;

  for (size_t size = 64; size <= TEST_SIZE; size <<= 1){
    for (size_t i = 0; i < 200; ++i) {
      auto before_time = std::chrono::high_resolution_clock::now();
      #ifdef SHUFFLE
      float res = weighted_avg(size, weights);
      #else
      float res = weighted_avg(size, weights, values);
      #endif
      auto after_time = std::chrono::high_resolution_clock::now();

      times[i] = std::chrono::duration<double, std::ratio<1, 1000>>(after_time - before_time).count();
      total_res += res;
      // std::cout << res << "," << std::chrono::duration<double, std::ratio<1, 1000>>(after_time - before_time).count() << std::endl;
    }

    double avg_time = 0;
    for (size_t i = 0; i < 200; ++i) {
      avg_time += times[i];
    }
    std::cout << size << "," << (avg_time / 200.0) << "," << total_res << std::endl;
    total_res = 0;

  }

  // #ifdef SHUFFLE
  // free(weights);
  // #endif

}
