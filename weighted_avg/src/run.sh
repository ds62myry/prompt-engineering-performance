#bin/bash

cmake ..
make -j

echo "Running base test"
./base | tee base.dat

echo "Running compiler optimized test"
./compiler | tee compiler.dat

echo "Running manualy optimized test"
./manual | tee manual.dat

echo "Running test with shuffled data"
./shuffle | tee shuffle.dat

echo "Running Naive test"
./naive | tee naive.dat

echo "Running Naive with compiler options"
./naive_compiler | tee naive_compiler.dat

echo "Running chain of thought test"
./cot | tee cot.dat

echo "Running expert test"
./expert | tee expert.dat
